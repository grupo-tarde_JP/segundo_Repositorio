# **BIOGRAFÍA** 

![mi fotografía](https://en.gravatar.com/userimage/143309901/d206d954dd6ddbf29ad8144f88e6d465.jpg?size=200) 

Soy **Jonathan Parra**. Nací el 01 de Julio de 1995, en la ciudad Quito. Vivo en Conocoto, sector Valle de los Chillos. Actualmente, estudio en la Escuela Politecnica Nacional en la Facultad de Sistemas y Computación, estoy cursando quinto semestre. En mis tiempos libres me gusta jugar fútbol y videojuegos en especial FIFA y GTA.