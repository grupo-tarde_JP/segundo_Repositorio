# **REPORTE**
------------------------------

## **USUARIO GUEST**
    * como usuario guest me permite clonar a cualquier cuenta de GitLab y poder ver las carpetas y archivos que se encuentran en el repositorio.
    * como usuario guest no me permite crear, ni modificar un archivo clonado.
    * como usuario guest no me permite crear una nueva rama, por lo consiguiente no podre realizar un merge en futuro.
## **USUARIO REPORTER**
    * como usuario reporter me permite clonar a cualquier cuenta de GitLab y poder ver las carpetas y archivos que se encuentran en el repositorio.
    * como usuario reporter no me permite crear, ni modificar un archivo clonado.
    * como usuario reporter no me permite crear una nueva rama, por lo consiguiente no podre realizar un merge en futuro.
    * como usuario reporter puedo enviar solicitud de issues, pero no puedo enviar solicitud de merge.
## **USUARIO DEVELOPER**
    * como usuario developer me permite clonar a cualquier cuenta de GitLab y poder ver las carpetas y archivos que se encuentran en el repositorio pero con la rama que cree.
    * como usuario developer me permite crear una nueva  rama.
    * como usuario developer si quiero crear algún archivo con una rama que no ha sido creada por mi, no podré crear, modificar archivos.
    * como usuario developer me permite crear y modificar un archivo clonado con la rama que cree anteriormente.
    * como usuario developer puedo enviar solicitud de issues, pero no puedo enviar solicitud de merge.
    * como usuario developer puedo buscar, más no agregar miembros al repositorio clonado.
## **USUARIO MAINTAINER**
    * como usuario maintainer me permite clonar a cualquier cuenta de GitLab y poder ver las carpetas y archivos que se encuentran en el repositorio pero con la rama que cree.
    * como usuario maintainer me permite crear una nueva  rama.
    * como usuario maintainer me permite crear y modificar un archivo clonado. 
    * como usuario maintainer puedo enviar solicitud de issues y realizar merge.
    * como usuario maintainer puedo buscar y agregar miembros al repositorio clonado.
    * como usuario maintainer puedo dar los permisos a los miembros 
    *  como usuario maintainer puedo realizar, peticiones y aceptaciones de merge.
  
--------------------
#### *Nota:* todos los cambios se los puede realizar de forma local en el caso de usuario guest y reporter, más no enviar al repositorio.
